<?php // don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
?>

</main>
<!-- Main END -->


<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-lg- offset-lg-4">
				<hr>
				<ul>
					<li>
						<?php _e( 'Greco Remodeling Services', 'greco_remodeling' ); ?><br class="hidden-md hidden-lg">
					</li>
					<li>
						<?php _e( '637 Frazier Ave. Unit 4, Elgin, IL 60123', 'greco_remodeling' ); ?><br
							class="hidden-md hidden-lg">
					</li>
					<li>
						<?php _e( '<a href="https://www.facebook.com/GrecoExteriors" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i>&nbsp;Facebook.com/GrecoExteriors', 'greco_remodeling' ); ?></a>
						<br class="hidden-md hidden-lg">
					</li>
					<li>
						<?php _e( 'We are fully licensed, insured and bonded.', 'greco_remodeling' ); ?><br
							class="hidden-md hidden-lg">
					</li>
				</ul>
				</ul>
			</div>
		</div>
<?php wp_footer(); ?>
</body>
</html>