<?php
/**
 * Template Name: Front Page Template
 *
 * @package    WordPress
 * @subpackage Greco Remodeling Theme
 * @since      Greco Remodeling Theme 1.0
 */

// don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

get_header();

get_template_part( 'template-parts/services-part' );

get_template_part( 'template-parts/license-part' );

get_footer();