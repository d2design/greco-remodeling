<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="theme-color" content="#ffde00">
        <link rel="icon" sizes="192x192" href="<?php echo esc_url( get_template_directory_uri() ); ?>/#">
        <link rel="shortcut icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/#">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-34280171-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-34280171-1');
        </script>


        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
    </head>
    <body>
        <!-- NAVBAR -->
        <!-- TOP BAR LG MD SM -->
        <div class="container navtop hidden-xs">
            <div class="row">
                <div class="col-md-2 col-xs-3">
                    <a href="<?php echo get_home_url(); ?>">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/logo/greco_800px.png" class="img-responsive logo">
                    </a>
                </div>
                <div class="col-sm-2 pull-right mt-10  hidden-sm hidden-md">
                    <a href="<?php echo esc_url( get_home_url() . '/contact'); ?>" class="as-button"><span class="btn btn-default"><?php _e( 'GET A QUOTE', 'greco_remodeling' ); ?></span></a>
                </div>
                <div class="col-xs-8 col-md-8 right">
	                <?php
	                wp_nav_menu( array(
			                'menu'            => 'top_nav',
			                'theme_location'  => 'top_nav',
			                'depth'           => 0,
			                'container'       => 'div',
			                'container_class' => 'collapse navbar-collapse',
			                'container_id'    => 'bs-example-navbar-collapse-1',
			                'menu_class'      => 'nav navbar-nav navbar-right',
			                'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
			                'walker'          => new WP_Bootstrap_Navwalker()
		                )
	                );
	                ?>
                </div>
            </div>
        </div>
        <!-- END TOP BAR LG MD SM  -->
        <!-- TOP BAR XS -->
        <div class="container hidden-lg hidden-md hidden-sm">
            <div class="row">
                <div class="col-xs-offset-4 col-xs-4 pad20">
					<a href="<?php echo get_home_url(); ?>">
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/logo/greco_800px.png" class="img-responsive" alt="Greco Remodeling Services">
					</a>
                </div>
            </div>
        </div>

        <nav class="navbar navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only"><?php _e( 'Toggle navigation', 'greco_remodeling' ); ?></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
	            <?php
	            wp_nav_menu( array(
			            'menu'            => 'main_menu',
			            'theme_location'  => 'main_menu',
			            'depth'           => 0,
			            'container'       => 'div',
			            'container_class' => 'collapse navbar-collapse',
			            'container_id'    => 'navbar',
			            'menu_class'      => 'nav navbar-nav navbar-right',
			            'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
			            'walker'          => new WP_Bootstrap_Navwalker()
		            )
	            ); ?>
            </div>
        </nav>

		<?php
		if ( is_front_page() && is_home() )  {
			get_template_part( 'template-parts/slider-part' );
		} elseif ( is_page() ) {
			get_template_part( 'template-parts/blueprint-part' );
		} else {
		//everything else
		}
		?>

		<!-- QUOTE BUTTON -->
		<div class="container padtop40">
			<div class="row">
				<div class="col-xs-12">
					<a href="<?php echo esc_url( get_home_url() . '/contact'); ?>" class="as-button hidden-lg hidden-md hidden-sm"><span
							class="btn btn-default btn-block">GET A QUOTE</span></a>
					<div class="padbottom20 hidden-lg hidden-md hidden-sm"></div>
				</div>
			</div>
		</div>

        <main>
