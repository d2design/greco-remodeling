<?php
/**
 * Partial: Front Page Slider
 */

// don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
?>



		<div class="col-md-8">
			<?php
			// Get the video URL and put it in the $video variable
			$videoID       = get_post_meta( $post->ID, 'video_url', true );
			$video_options = 'frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen';
			// Check if there is in fact a video URL
			if ( $videoID ) {

				echo '<div class="js-video vimeo widescreen">';
				// Echo the embed code via oEmbed
				echo wp_oembed_get( 'http://player.vimeo.com/video/' . $videoID . $video_options );
				echo '</div>';

			}
			?>
		</div>

