<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/21/17
 * Time: 10:54 AM
 */
?>

<?php


function supplier_logos( $logo_category ) {

	$args = array(
		'post_type'      => 'attachment',
		'category__in'   => $logo_category,
		'post_mime_type' => 'image',
		'post_status'    => 'inherit',
		'posts_per_page' => - 1,
		'orderby'        => 'title',
		'order'          => 'ASC',

	);
	$query_images = new WP_Query( $args );
	$images       = array();
	foreach ( $query_images->posts as $image ) {
		$images[] = $image->guid;
	}

	return $images;
}

function get_suppliers( $logo_category ) {

	$imgs        = supplier_logos( $logo_category );

	$html = '';

	$title = '<h4 >' . esc_html( 'Suppliers', 'greco_remodeling' ) . '</h4>';

	$html_before = '<section>';
	$html_before .= '<div class="container">' . '<div class="row">' . '<div class="col-xs-12">' . '<div id="suppliers">';
	$html_before .= $title;

	$html_after = '</div>' . '</div>' . '</div>' . '</div>' . '</section>';

	foreach ( $imgs as $img ) {

		$html .= '<div class="col-xs-6 col-sm-6 col-lg-2">' . '<img src="' . $img . '">' . '</div>';

	}

	return $html_before . $html . $html_after;
}

function display_suppliers() {
	if ( is_page( 'roofing' ) ) {
		// the page is "Roofing", or the parent of the page is "Roofing"
		echo get_suppliers( 4 );
	} elseif ( is_page( 'siding' ) ) {
		// the page is "Roofing", or the parent of the page is "Roofing"
		echo get_suppliers( 7 );
	} elseif ( is_page( 'windows' ) ) {
		// the page is "Roofing", or the parent of the page is "Roofing"
		echo get_suppliers( 3 );
	} elseif ( is_page( 'doors' ) ) {
		// the page is "Roofing", or the parent of the page is "Roofing"
		echo get_suppliers( 23 );
	}

}

display_suppliers();
?>
