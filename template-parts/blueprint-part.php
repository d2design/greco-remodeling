<?php
/**
 * Partial: Blueprint Graphic
 *
 */

// don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
?>


<div class="headerblueprint">
	<div class="container">
		<div class="row">
			<div class="col-xs-12"></div>
		</div>
	</div>
</div>
