<?php
/**
 * Partial: Front Page Slider
 */

// don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
?>

<section>
    <div id="carousel-example-generic" class="carousel slide hidden-xs" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="<?php echo get_theme_mod( 'slider_image_1', 'featured-slider' ); ?>" alt="<?php echo get_theme_mod( 'slider_title_1' ); ?>">
                <div class="container">
                    <div class="carousel-caption">
                        <h3><?php echo get_theme_mod( 'slider_title_1' ); ?></h3>
                        <p><?php echo get_theme_mod( 'slider_description_1' ); ?></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo get_theme_mod( 'slider_image_2', 'featured-slider' ); ?>" alt="<?php echo get_theme_mod( 'slider_title_2' ); ?>">
                <div class="container">
                    <div class="carousel-caption">
                        <h3><?php echo get_theme_mod( 'slider_title_2' ); ?></h3>
                        <p><?php echo get_theme_mod( 'slider_description_2' ); ?></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="<?php echo get_theme_mod( 'slider_image_3', 'featured-slider' ); ?>" alt="<?php echo get_theme_mod( 'slider_title_3' ); ?>">
                <div class="container">
                    <div class="carousel-caption">
                        <h3><?php echo get_theme_mod( 'slider_title_3' ); ?></h3>
                        <p><?php echo get_theme_mod( 'slider_description_3' ); ?></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
</section>