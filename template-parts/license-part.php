<?php
/**
 * Partial: License part
 */

// don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 padtop40">
                <h2 class="text-center"><?php _e( 'Fully Licensed and Insured', 'greco_remodeling' ); ?></h2>
            </div>
        </div>
        <div class="row">
			<div class="col-lg-12 text-center">
				<div id="license">
					<?php dynamic_sidebar( 'license-widget' ); ?>
				</div>
			</div>
        </div>
    </div>
</section>