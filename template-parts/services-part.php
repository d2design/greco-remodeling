<?php
/**
 * Partial: Services part
 */

// don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
?>

<section id="services">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="text-center">More reasons to choose Greco Remodeling Services</h2>
			</div>
		</div>
		<?php

		echo '<div class="row">';

		$i = 1; // Count to return a new 3-col row

		$args = array(
			'post_type'     => 'page',
			'category_name' => 'services',
			'order' 		=> 	'ASC',
			'orderby' 		=> 'menu_order',
		);

		$query = new WP_Query ( $args );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post(); ?>

				<div class="col-sm-4 padtop20">
					<div class="rounded">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'featured-small', 'class="img-responsive"'); ?></a>
					</div>
					<h3 class="text-center"><a href="<?php the_permalink(); ?>" rel="bookmark"
											   title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
					<p><?php the_excerpt('text-center'); ?></p>
				</div>
				<?php
				// After 3 close the row div and open a new one
				if ( $i % 3 == 0 ) {
					echo '</div><div class="col-sm-4 padtop20">';
				}
			}
		}
		?>
</section>
