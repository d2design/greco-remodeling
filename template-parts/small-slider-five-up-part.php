<?php
/**
 * Partial: Small Slider 5 Slides
 */

// don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
?>


		<div class="col-md-8">
			<!-- Carousel -->
			<div id="carousel-example-generic" class="carousel slide rounded outlined" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
					<li data-target="#carousel-example-generic" data-slide-to="3"></li>
					<li data-target="#carousel-example-generic" data-slide-to="4"></li>
				</ol>
				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
						<?php the_post_thumbnail( 'sm-slider' ); ?>
					</div>
					<div class="item">
						<?php kdmfi_the_featured_image( 'featured-image-2', 'sm-slider' ); ?>
					</div>
					<div class="item">
						<?php kdmfi_the_featured_image( 'featured-image-3', 'sm-slider' ); ?>
					</div>
					<div class="item">
						<?php kdmfi_the_featured_image( 'featured-image-4', 'sm-slider' ); ?>
					</div>
					<div class="item">
						<?php kdmfi_the_featured_image( 'featured-image-5', 'sm-slider' ); ?>
					</div>

				</div>

				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
		</div>