<?php
/**
 * Partial: Content
 */

// don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

?>

<?php
	add_filter( 'gform_field_container', 'add_bootstrap_container_class', 10, 6 );
	function add_bootstrap_container_class( $field_container, $field, $form, $css_class, $style, $field_content ) {
	$id = $field->id;
	$field_id = is_admin() || empty( $form ) ? "field_{$id}" : 'field_' . $form['id'] . "_$id";
	return '
	<li id="' . $field_id . '" class="' . $css_class . ' form-group">{FIELD_CONTENT}</li>';
	}
?>
<?php if ( have_posts() ) : while ( have_posts() ) :
	the_post(); ?>

	<div class="col-md-4">
		<div class="padtop20 hidden-lg hidden-md hidden-sm"></div>
		<h1><?php the_title(); ?></h1>
		<?php the_content(); ?>
	</div>
<?php endwhile; else: ?>
	<?php _e( 'Sorry, no posts matched your criteria.', 'greco_remodeling' ); ?>
<?php endif; ?>