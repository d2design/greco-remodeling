<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package    WordPress
 * @subpackage Greco Remodeling Theme
 * @since      Greco Remodeling Theme 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
					<div class="col-xs-12">
					<h1 class="text-center"><?php _e( 'Oops! That page can&rsquo;t be found.', 'greco_remodeling' ); ?></h1>
					</div>
			</section><!-- .error-404 -->
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();
