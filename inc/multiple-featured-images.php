<?php
/**
 * Load multiple featured images on slider page template
 *
 * @package greco_remodeling
 * @since   greco_remodeling 1.0.0
 */

// don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

add_filter( 'kdmfi_featured_images', function ( $featured_images ) {
	$args_1 = array(
		'id'           => 'featured-image-2',
		'desc'         => 'For second slider image on slider template.',
		'label_name'   => 'Featured Image 2',
		'label_set'    => 'Set featured image 2',
		'label_remove' => 'Remove featured image 2',
		'label_use'    => 'Set featured image 2',
		'post_type'    => array( 'page' ),
	);

	$args_2 = array(
		'id'           => 'featured-image-3',
		'desc'         => 'For third slider image on slider template.',
		'label_name'   => 'Featured Image 3',
		'label_set'    => 'Set featured image 3',
		'label_remove' => 'Remove featured image 3',
		'label_use'    => 'Set featured image 3',
		'post_type'    => array( 'page' ),
	);

	$args_3 = array(
		'id'           => 'featured-image-4',
		'desc'         => 'For third slider image on slider template.',
		'label_name'   => 'Featured Image 4',
		'label_set'    => 'Set featured image 4',
		'label_remove' => 'Remove featured image 4',
		'label_use'    => 'Set featured image 4',
		'post_type'    => array( 'page' ),
	);
	$args_4 = array(
		'id'           => 'featured-image-5',
		'desc'         => 'For third slider image on slider template.',
		'label_name'   => 'Featured Image 5',
		'label_set'    => 'Set featured image 5',
		'label_remove' => 'Remove featured image 5',
		'label_use'    => 'Set featured image 5',
		'post_type'    => array( 'page' ),
	);

	$featured_images[] = $args_1;
	$featured_images[] = $args_2;
	$featured_images[] = $args_3;
	$featured_images[] = $args_4;

	return $featured_images;
} );