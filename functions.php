<?php
/**
 * Functions File
 *
 * @package greco_remodeling
 * @since   greco_remodeling 1.0.0
 */

// don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}


/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Various clean up functions */
require_once( 'library/customizer.php' );

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/* Include Resources */
require_once "inc/wp_bootstrap_navwalker.php";

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/* Include Editor Style Resources */
require_once "inc/editor-style.php";

/** Page categories */
require_once( 'library/page-categories.php' );

/** Configure responsive image sizes */
require_once( 'library/responsive-images.php' );

/** Configure Multiple images */
require_once( 'inc/multiple-featured-images.php' );
