<?php
/**
 * Add categories to pages
 *
 * @package greco_remodeling
 * @since   greco_remodeling 1.0.0
 */

// don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

function greco_remodeling_add_categories_to_pages() {
	register_taxonomy_for_object_type( 'category', 'page' );
}

add_action( 'init', 'greco_remodeling_add_categories_to_pages' );

if ( ! is_admin() ) {
	add_action( 'pre_get_posts', 'category_and_tag_archives' );

}
function category_and_tag_archives( $wp_query ) {
	$my_post_array = array( 'post', 'page' );

	if ( $wp_query->get( 'category_name' ) || $wp_query->get( 'cat' ) ) {
		$wp_query->set( 'post_type', $my_post_array );
	}
}