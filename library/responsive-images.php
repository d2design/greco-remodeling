<?php
/**
 * Configure responsive images sizes
 *
 * @package    WordPress
 * @subpackage greco_remodeling
 * @since      greco_remodeling 1.0
 */

// Add featured image sizes
//
// Sizes are optimized and cropped for landscape aspect ratio
// and optimized for HiDPI displays on 'small' and 'medium' screen sizes.

add_image_size( 'featured-small', 640, 220, true ); // name, width, height, crop
add_image_size( 'featured-medium', 1280, 400, true );
add_image_size( 'featured-large', 1440, 400, true );
add_image_size( 'featured-xlarge', 1920, 400, true );
add_image_size( 'featured-xsmall', 360, 220, true );
add_image_size( 'featured-slider', 2880, 1200, true );
// Add additional image sizes
add_image_size( 'small-logo', 300 );
add_image_size( 'fp-small', 640 );
add_image_size( 'fp-medium', 1024 );
add_image_size( 'fp-large', 1200 );
add_image_size( 'fp-xlarge', 2880 );
add_image_size( 'sm-slider', 760 );
//set_post_thumbnail_size( 825, 510, true );


// Register the new image sizes for use in the add media modal in wp-admin
function greco_remodeling_custom_sizes( $sizes ) {
	return array_merge( $sizes, array(
		'fp-small'        => __( 'FP Small - 640px' ),
		'fp-medium'       => __( 'FP Medium - 1024px' ),
		'fp-large'        => __( 'FP Large - 1200px' ),
		'fp-xlarge'       => __( 'FP XLarge - 2880px' ),
		'featured-slider' => __( 'Slider Image 2880 x 1200' ),
		'sm-slider'       => __( 'Small Slider Image - 760px' ),
		'small-logo'      => __( 'Small Logo 300px' ),
	) );
}

add_filter( 'image_size_names_choose', 'greco_remodeling_custom_sizes' );

// Add custom image sizes attribute to enhance responsive image functionality for content images
function greco_remodeling_adjust_image_sizes_attr( $sizes, $size ) {

	// Actual width of image
	$width = $size[0];

	// Full width page template
	if ( is_page_template( 'page-templates/page-full-width.php' ) ) {
		if ( 2880 < $width ) {
			$sizes = '(max-width: 1199px) 98vw, 1200px';
		} else {
			$sizes = '(max-width: 1199px) 98vw, ' . $width . 'px';
		}
	} else { // Default 3/4 column post/page layout
		if ( 770 < $width ) {
			$sizes = '(max-width: 639px) 98vw, (max-width: 1199px) 64vw, 770px';
		} else {
			$sizes = '(max-width: 639px) 98vw, (max-width: 1199px) 64vw, ' . $width . 'px';
		}
	}

	return $sizes;
}

add_filter( 'wp_calculate_image_sizes', 'greco_remodeling_adjust_image_sizes_attr', 10, 2 );

// Remove inline width and height attributes for post thumbnails
function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
	$html = preg_replace( '/(width|height)=\"\d*\"\s/', '', $html );

	return $html;
}

add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );
