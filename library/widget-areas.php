<?php
/**
 * Load multiple featured images on slider page template
 *
 * @package greco_remodeling
 * @since   greco_remodeling 1.0.0
 */

// don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

// Register widget areas.
if ( ! function_exists( 'greco_remodeling_widgets_init' ) ) :

	function greco_remodeling_widgets_init() {

		register_sidebar( array(
			'id'            => 'license-widget',
			'name'          => __( 'License Section', 'greco_remodeling' ),
			'description'   => __( 'Homepage License Section', 'greco_remodeling' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<h3>',
			'after_title'   => '<h3>',
		) );

	}

	add_action( 'widgets_init', 'greco_remodeling_widgets_init' );
endif;