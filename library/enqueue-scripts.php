<?php
/**
 * Enqueue all styles and scripts - Author: Ole Fredrik Lie
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package greco_remodeling
 * @since   greco_remodeling 1.0.0
 *
 */

/* Enqueue Scripts Begin */
if ( ! function_exists( 'greco_remodeling_enqueue_scripts' ) ) :
	function greco_remodeling_enqueue_scripts() {


		wp_deregister_script( 'jquery' );
		wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', false, null, true );

		wp_deregister_script( 'bootstrap' );
		wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/bootstrap-3.3.7-dist/js/bootstrap.min.js', false, null, true );


		/* Enqueue Styles */

		wp_deregister_style( 'bootstrap' );
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/bootstrap-3.3.7-dist/css/bootstrap.min.css', false, null, 'all' );

		wp_deregister_style( 'style-1' );
		wp_enqueue_style( 'style-1', 'https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700', false, null, 'all' );

		wp_deregister_style( 'styles' );
		wp_enqueue_style( 'styles', get_template_directory_uri() . '/css/styles.css', false, null, 'all' );

		wp_deregister_style( 'fontawesome' );
		wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/assets/font-awesome-4.7.0/css/font-awesome.min.css', false, null, 'all' );

	}


	add_action( 'wp_enqueue_scripts', 'greco_remodeling_enqueue_scripts' );
endif;