<?php
/**
 * Contains methods for customizing the theme customization screen.
 *
 */

function greco_remodeling_customizer( $wp_customize ) {

	$wp_customize->add_section( 'slider', array(
		'title'       => __( 'Slider', 'greco_remodeling' ),
		'priority'    => 60,
		'description' => __( 'Slider Image and Description' ), // optional
		'active_callback' => 'is_front_page',
	) );

	$wp_customize->add_setting( 'slider_image_1', array(
		'default'   => 'https://via.placeholder.com/2880x1200',
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'slider_image_1',
			array(
				'label'    => __( 'Slider Image 1', 'greco_remodeling' ),
				'section'  => 'slider',
				'settings' => 'slider_image_1',
				'context'  => 'hero-image',
			)
		) );

	$wp_customize->add_setting( 'slider_title_1', array(
		'default'   => __( 'Slider Title', 'greco_remodeling' ),
		'transport' => 'refresh',
		//'sanitize_callback' => 'customizer_text_sanitizer',
	) );

	$wp_customize->add_control( 'slider_title_1', array(
		'label'    => __( 'Slider Title 1', 'greco_remodeling' ),
		'section'  => 'slider',
		'settings' => 'slider_title_1',
		'type'     => 'text',
	) );

	$wp_customize->add_setting( 'slider_description_1', array(
		'default'   => __( 'Slider Description', 'greco_remodeling' ),
		'transport' => 'refresh',
		//'sanitize_callback' => 'customizer_text_sanitizer',
	) );

	$wp_customize->add_control( 'slider_description_1', array(
		'label'    => __( 'Slider Description 1', 'greco_remodeling' ),
		'section'  => 'slider',
		'settings' => 'slider_description_1',
		'type'     => 'text',
	) );



	$wp_customize->add_setting( 'slider_image_2', array(
		'default'   => 'https://via.placeholder.com/2880x1200',
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'slider_image_2',
			array(
				'label'    => __( 'Slider Image 2', 'greco_remodeling' ),
				'section'  => 'slider',
				'settings' => 'slider_image_2',
				'context'  => 'hero-image',
			)
		) );

	$wp_customize->add_control( 'slider_title_2', array(
		'label'    => __( 'Slider Title 2', 'greco_remodeling' ),
		'section'  => 'slider',
		'settings' => 'slider_title_2',
		'type'     => 'text',
	) );

	$wp_customize->add_setting( 'slider_title_2', array(
		'default'   => __( 'Slider Title 2', 'greco_remodeling' ),
		'transport' => 'refresh',
		//'sanitize_callback' => 'customizer_text_sanitizer',
	) );

	$wp_customize->add_control( 'slider_title_2', array(
		'label'    => __( 'Slider Title 2', 'greco_remodeling' ),
		'section'  => 'slider',
		'settings' => 'slider_title_2',
		'type'     => 'text',
	) );

	$wp_customize->add_setting( 'slider_description_2', array(
		'default'   => __( 'Slider Description 2', 'greco_remodeling' ),
		'transport' => 'refresh',
		//'sanitize_callback' => 'customizer_text_sanitizer',
	) );

	$wp_customize->add_control( 'slider_description_2', array(
		'label'    => __( 'Slider Description 2', 'greco_remodeling' ),
		'section'  => 'slider',
		'settings' => 'slider_description_2',
		'type'     => 'text',
	) );

	$wp_customize->add_setting( 'slider_image_3', array(
		'default'   => 'https://via.placeholder.com/2880x1200',
		'transport' => 'refresh',
	) );

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'slider_image_3',
			array(
				'label'    => __( 'Slider Image 3', 'greco_remodeling' ),
				'section'  => 'slider',
				'settings' => 'slider_image_3',
				'context'  => 'hero-image',
			)
		) );

	$wp_customize->add_control( 'slider_title_3', array(
		'label'    => __( 'Slider Title 3', 'greco_remodeling' ),
		'section'  => 'slider',
		'settings' => 'slider_title_3',
		'type'     => 'text',
	) );

	$wp_customize->add_setting( 'slider_title_3', array(
		'default'   => __( 'Slider Title 3', 'greco_remodeling' ),
		'transport' => 'refresh',
		//'sanitize_callback' => 'customizer_text_sanitizer',
	) );

	$wp_customize->add_control( 'slider_title_3', array(
		'label'    => __( 'Slider Title 3', 'greco_remodeling' ),
		'section'  => 'slider',
		'settings' => 'slider_title_3',
		'type'     => 'text',
	) );

	$wp_customize->add_setting( 'slider_description_3', array(
		'default'   => __( 'Slider Description 3', 'greco_remodeling' ),
		'transport' => 'refresh',
		//'sanitize_callback' => 'customizer_text_sanitizer',
	) );

	$wp_customize->add_control( 'slider_description_3', array(
		'label'    => __( 'Slider Description 3', 'greco_remodeling' ),
		'section'  => 'slider',
		'settings' => 'slider_description_3',
		'type'     => 'text',
	) );
}

	function customizer_text_sanitizer( $text ) {
		return esc_text( $text );
	}





add_action( 'customize_register', 'greco_remodeling_customizer' );