<?php

if ( ! function_exists( 'greco_remodeling_init' ) ) :

	function greco_remodeling_init() {


		// Use categories and tags with attachments
		register_taxonomy_for_object_type( 'category', 'attachment' );
		register_taxonomy_for_object_type( 'post_tag', 'attachment' );

	}
endif;

add_action( 'init', 'greco_remodeling_init' );


if ( ! function_exists( 'greco_remodeling_setup' ) ) :

	function greco_remodeling_setup() {

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 */
		/* Pinegrow generated Load Text Domain Begin */
		load_theme_textdomain( 'greco_remodeling', get_template_directory() . '/languages' );
		/* Pinegrow generated Load Text Domain End */

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Add support for excerpt on pages
		 */

		add_post_type_support( 'page', 'excerpt' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 */
		add_theme_support( 'post-thumbnails' );
		//set_post_thumbnail_size( 825, 510, true );

		// Add menus.
		register_nav_menus( array(
			'top_nav'   => __( 'Top Nav', 'greco_remodeling' ),
			'main_menu' => __( 'Main Menu', 'greco_remodeling' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'widgets',
		) );

		/*
		 * Enable support for Post Formats.
		 */
		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
			'gallery',
			'status',
			'audio',
			'chat',
		) );

		// Add support for excerpt on pages
		add_post_type_support( 'page', 'excerpt' );
	}
endif; // greco_remodeling_setup

add_action( 'after_setup_theme', 'greco_remodeling_setup' );