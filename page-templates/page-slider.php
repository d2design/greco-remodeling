<?php
/**
 * Template Name: Right Slider Page Template
 *
 * @package    WordPress
 * @subpackage Greco Remodeling Theme
 * @since      Greco Remodeling Theme 1.0
 */

// don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
?>

<?php get_header(); ?>
	<section>
		<div class="container">
			<div class="row">
				<?php get_template_part( 'template-parts/content-part' ); ?>
				<?php get_template_part( 'template-parts/small-slider-part' ); ?>
			</div>
		</div>
	</section>
<?php get_template_part( 'template-parts/logos-part' ); ?>
<?php get_footer(); ?>

