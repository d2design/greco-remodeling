<?php
/**
 * Template Name: Contact Page Template
 *
 * @package    WordPress
 * @subpackage Greco Remodeling Theme
 * @since      Greco Remodeling Theme 1.0
 */

// don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
?>

<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) :
the_post(); ?>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-4 padtop40">
					<?php the_content(); ?>
				</div>
				<div class="col-md-8">
					<div class="padtop20 hidden-sm hidden-xs"></div>
						<hr class="hidden-lg hidden-md">
						<?php echo do_shortcode( '[gravityform id="3" title="false" description="true"]' ); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endwhile; else: ?>
	<?php _e( 'Sorry, no posts matched your criteria.', 'greco_remodeling' ); ?>
<?php endif; ?>
</div>
<?php get_footer(); ?>

