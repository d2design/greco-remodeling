<?php
/**
 * Template Name: Full Width Page
 *
 * @package    WordPress
 * @subpackage Greco Remodeling Theme
 * @since      Greco Remodeling Theme 1.0
 */

// don't allow direct access to this file
if ( ! function_exists( 'add_filter' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}
?>
<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) :
	the_post(); ?>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div class="padtop20 hidden-lg hidden-md hidden-sm"></div>
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</section>

<?php endwhile; else: ?>
	<?php _e( 'Sorry, no posts matched your criteria.', 'greco_remodeling' ); ?>
<?php endif; ?>

<?php get_footer(); ?>